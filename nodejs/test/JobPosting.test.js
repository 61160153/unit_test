const { checkEnableTime } = require("../JobPosting")
const assert = require('assert')
describe('JobPosting', function () {
    describe("TimeTodayIsBetweenStartTimeAndEndTime", function () {
        it('shoud return true when เวลาสมัครอยู่ในช่วงเวลาระหว่างเวลาเริ่มต้นและเวลาสิ้นสุด', function () {
            //Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 2, 3)
            const expectedResult = true;
            // Act
            const actualResult = checkEnableTime(startTime, endTime, today)

            //Assert
            assert.strictEqual(actualResult, expectedResult)
            
        })
    })
    // if (actualResult === expectedResult) {
    //     console.log('เวลาระหว่าง เริ่มต้นและสิ้นสุด PASSID')
    // } else {
    //     console.log('เวลาระหว่าง เริ่มต้นและสิ้นสุด FAIL')
    // }
    describe("TimeTodayIsBeforeStartTime", function () {
        it('shoud return false when เวลาสมัครอยู่ก่อนเวลาเริ่มต้น', function () {
            //Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 1, 30)
            const expectedResult = false;
            // Act
            const actualResult = checkEnableTime(startTime, endTime, today)

            //Assert
            assert.strictEqual(actualResult, expectedResult)
        })
    })
    describe("TimeTodayIsAfterEndTime", function () {
        it('shoud return false when เวลาสมัครอยู่หลังเวลาสิ้นสุด', function () {
            //Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 2, 6)
            const expectedResult = false;
            // Act
            const actualResult = checkEnableTime(startTime, endTime, today)

            //Assert
            assert.strictEqual(actualResult, expectedResult)
        })
    })
    describe("TimeTodayIsEqualStartTime()", function () {
        it('shoud return true when เวลาสมัครเท่ากับเวลาเริ่มต้น', function () {
            //Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 1, 31)
            const expectedResult = true;
            // Act
            const actualResult = checkEnableTime(startTime, endTime, today)

            //Assert
            assert.strictEqual(actualResult, expectedResult)
        })
    })
    describe("TimeTodayIsEqualEndtTime()", function () {
        it('shoud return true when เวลาสมัครเท่ากับเวลาสิ้นสุด', function () {
            //Arrage
            const startTime = new Date(2021, 1, 31)
            const endTime = new Date(2021, 2, 5)
            const today = new Date(2021, 2, 5)
            const expectedResult = true;
            // Act
            const actualResult = checkEnableTime(startTime, endTime, today)

            //Assert
            assert.strictEqual(actualResult, expectedResult)
        })
    })

})


